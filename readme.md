# Hashedlogin: 

- A database that allows you to create an account and password. 
- Passwords are hashed in function bcrypt. 
- The user creates an account where he creates a username and password and is assigned a salt. 
- All info except username are hashed. 

# LIB:
- bcrypt 
- sqlite3

# Superviser entry:
- Username: karot
- Password: mrkev

# usefull info:
- https://www.programiz.com/python-programming/methods/built-in/hash
- https://cs.wikipedia.org/wiki/Bcrypt
