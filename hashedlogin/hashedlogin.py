import sys
import sqlite3
import getpass
import bcrypt
import time

def hashed(a,salt):
    hasher = a.encode()
    hashed = bcrypt.hashpw(hasher, salt)
    hashed = hashed.decode('utf-8')
    return hashed

def verifyadmin(password,hashed,cursor):
    if password == hashed:
        print("PASSWORD CORRECT!")
        cursor.execute('SELECT * FROM hashedlogin')
        sventry = cursor.fetchall()
        print("All INFO IN DB:")
        print(sventry)   
        main()
    else:
        print("WRONG PASSWORD!")
        time.sleep(2)
        main()

def verify(password,hashed,cursor,user):
    if password == hashed:
        print("PASSWORD CORRECT!")
        afterlog(cursor,user)
    else:
        print("WRONG PASSWORD!")
        time.sleep(2)
        main()


def login(cursor):
    while True:
        user = input("ENTER USERNAME: ")
        cursor.execute("SELECT Username from hashedlogin where Username=:a",{"a": user})
        if not cursor.fetchone(): 
            time.sleep(1)
            print("BAD USERNAME!")
            main()
        else:
            time.sleep(1)
            cursor.execute("SELECT Password from hashedlogin where Username=:b",{"b": user})
            hashed_in_db = cursor.fetchone()
            hashed_in_db = hashed_in_db[0].encode('utf-8')
            cursor.execute("SELECT Salt from hashedlogin where Username=:b",{"b": user})
            salt_in_db = cursor.fetchone()
            salt_in_db = salt_in_db[0].encode('utf-8')
            passwordinput = getpass.getpass()
            passwordinput = hashed(passwordinput,salt_in_db)
            passwordinput = passwordinput.encode('utf-8')
            verify(passwordinput,hashed_in_db,cursor,user)
            main()


def afterlog(cursor,user):
    afterlogin = input("1. SHOW HASHED INFO 2.BACK ")
    if afterlogin == "1":
        cursor.execute("SELECT Info from hashedlogin where Username=:b",{"b": user})
        show_info = cursor.fetchone()
        print(show_info)
        main()
    elif afterlogin == "2":
        main()
    else:
        print("BAD ENTRY! TRY IT AGAIN.")


def superviser(cursor,log):
        viser1 = input("1.CREATE NEW ADMIN 2.LOG AND SHOW DB ")
        if viser1 == "1":
            crtadmin(log,cursor)
        elif viser1 == "2":
            adminput = input("ENTER USERNAME: ")
            cursor.execute("SELECT Username from hashedlogin where Username=:a",{"a": adminput})
            if not cursor.fetchone(): 
                time.sleep(1)
                print("BAD USERNAME!")
                main()
            else:
                time.sleep(1)
                cursor.execute("SELECT Password from hashedlogin where Username=:b",{"b": adminput})
                hashed_in_db = cursor.fetchone()
                hashed_in_db = hashed_in_db[0].encode('utf-8')
                cursor.execute("SELECT Salt from hashedlogin where Username=:b",{"b": adminput})
                salt_in_db = cursor.fetchone()
                salt_in_db = salt_in_db[0].encode('utf-8')
                passwordinput = getpass.getpass()
                passwordinput = hashed(passwordinput,salt_in_db)
                passwordinput = passwordinput.encode('utf-8')
                verifyadmin(passwordinput,hashed_in_db,cursor)

        
def crtadmin(log,cursor):
    while True:
        usercrt = input("ENTER ADMIN USERNAME: ")
        passwordcrt = input("ENTER ADMIN PASSWORD: ")
        salt = bcrypt.gensalt()
        passwordcrthashed = hashed(passwordcrt,salt)
        salt = salt.decode('utf-8')
        addadmin = '''INSERT INTO hashedlogin(Username, Password, Salt)VALUES (?, ?, ?)'''
        cursor.execute(addadmin,[(usercrt), (passwordcrthashed),(salt)])
        log.commit()
        time.sleep(1)
        print("ADMIN USERNAME AND HASHED PASSWORD SAVED .")
        main()


def crtuser(log,cursor):
    while True:
        usercrt = input("ENTER NEW USERNAME: ")
        passwordcrt = input("ENTER NEW PASSWORD: ")
        info = input("ENTER INFO:")
        saltinfo = bcrypt.gensalt()
        hasheddinfo= hashed(info,saltinfo)
        salt = bcrypt.gensalt()
        passwordcrthashed = hashed(passwordcrt,salt)
        salt = salt.decode('utf-8')
        adduser(usercrt, passwordcrthashed,salt,hasheddinfo,saltinfo,log,cursor)
        log.commit()
        time.sleep(1)
        print("USERNAME AND HASHED PASSWORD AND HASHED INFO SAVED .")
        main()


def adduser(Username, Password,Salt,Info,Infosalt,log,cursor):
    add = '''INSERT INTO hashedlogin(Username, Password, Salt, Info, Infosalt)VALUES (?, ?, ?, ?, ?)'''
    cursor.execute(add,[(Username), (Password),(Salt),(Info),(Infosalt)])
    log.commit()


def main():
    while True:
        log = sqlite3.connect("hashedlogin.db")
        cursor = log.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS hashedlogin(Username TEXT, Password TEXT, Salt TEXT, Info TEXT, Infosalt Text)')
        menu = input("[1]-LOGIN [2]-CREATE USER [3]-SUPERVISER ENTRY [4]-EXIT ")
        if menu == "1":
            login(cursor)
        elif menu == "2":
            crtuser(log,cursor)
        elif menu == "3":
            superviser(cursor,log)
        elif menu == "4":
            break
        else: 
            print("WRONG INPUT.")


if __name__ == "__main__":
    sys.exit(main())


